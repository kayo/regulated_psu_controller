#ifndef __HANDLER__PIO_H__
#define __HANDLER__PIO_H__

#define _pio_fn(btn, op, ...) pio_##btn##_##op(__VA_ARGS__)
#define pio_fn(btn, op, ...) _pio_fn(btn, op, ##__VA_ARGS__)

#endif /* __HANDLER__PIO_H__ */
