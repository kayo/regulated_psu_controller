#ifndef __HANDLER__ENC_H__
#define __HANDLER__ENC_H__

#include <handler/pio.h>

#define _enc_vr(enc, var) _##enc##_##var
#define enc_vr(enc, var) _enc_vr(enc, var)

#define _enc_fn(enc, op, ...) enc_##enc##_##op(__VA_ARGS__)
#define enc_fn(enc, op, ...) _enc_fn(enc, op, ##__VA_ARGS__)

#define enc_ts(old, new) (((old) << 2) | (new))

#define enc_def(enc, on_ccw, on_cw) \
  static int8_t                     \
  enc_vr(enc, state);               \
  static int8_t                     \
  enc_vr(enc, count);               \
  static void                       \
  enc_fn(enc, init, void) {         \
    enc_vr(enc, state) =            \
      (pio_fn(enc, A_get) << 1)     \
      | pio_fn(enc, B_get);         \
    enc_vr(enc, count) = 0;         \
  }                                 \
  static void                       \
  enc_fn(enc, trig, void) {         \
    uint8_t _state_ =               \
      (pio_fn(enc, A_get) << 1)     \
      | pio_fn(enc, B_get);         \
                                    \
    switch (enc_ts(                 \
             enc_vr(enc, state),    \
             _state_)) {            \
    case enc_ts(1, 0):              \
    case enc_ts(3, 1):              \
    case enc_ts(2, 3):              \
    case enc_ts(0, 2):              \
      enc_vr(enc, count) --;        \
      break; /* cw */               \
    case enc_ts(0, 1):              \
    case enc_ts(1, 3):              \
    case enc_ts(3, 2):              \
    case enc_ts(2, 0):              \
      enc_vr(enc, count) ++;        \
      break; /* ccw */              \
    default:                        \
      return;                       \
    }                               \
                                    \
    enc_vr(enc, state) = _state_;   \
                                    \
    switch (enc_vr(enc, count)) {   \
    case -4:                        \
      on_cw;                        \
      break;                        \
    case 4:                         \
      on_ccw;                       \
      break;                        \
    default:                        \
      return;                       \
    }                               \
                                    \
    enc_vr(enc, count) = 0;         \
  }                                 \
  void                              \
  pio_fn(enc, A_on_edge, void) {    \
    enc_fn(enc, trig);              \
  }                                 \
  void                              \
  pio_fn(enc, B_on_edge, void) {    \
    enc_fn(enc, trig);              \
  }                                 \

#endif /* __HANDLER__ENC_H__ */
