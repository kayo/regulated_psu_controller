#ifndef __HANDLER__TCS_H__
#define __HANDLER__TCS_H__

#include <stdint.h>

typedef int32_t tcs_t;

tcs_t tcs_read(void);
void tcs_incr(tcs_t time);
tcs_t tcs_diff(tcs_t from, tcs_t to);

#endif /* __HANDLER__TCS_H__ */
