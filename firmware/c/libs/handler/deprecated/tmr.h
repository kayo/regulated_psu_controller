#ifndef __HANDLER__TMR_H__
#define __HANDLER__TMR_H__

#include <handler/tcs.h>
#include <stdbool.h>

typedef struct {
  tcs_t end;
  bool set;
} tmr_t;

static inline void tmr_set(tmr_t *timer, tcs_t timeout) {
  timer->end = tcs_read() + timeout;
  timer->set = true;
}

/* returns true when timer is reached */
bool tmr_has(tmr_t *timer);

#define tmr_on(timer, on_out)  \
  if (tmr_has(timer)) {        \
    on_out;                    \
  }

#endif /* __HANDLER__TMR_H__ */
