#include <handler/tmr.h>

bool tmr_has(tmr_t *timer) {
  if (timer->set &&
      tcs_diff(tcs_read(),
               timer->end)
      <= 0) {
    timer->set = false;
    return true;
  }

  return false;
}
