#include <handler/tcs.h>

#define TCS_MAX INT32_MAX

static tcs_t tcs_time = 0;

tcs_t tcs_read(void) {
  return tcs_time;
}

void tcs_incr(tcs_t time) {
  tcs_time = TCS_MAX - time >= tcs_time ? tcs_time + time : time - (TCS_MAX - tcs_time);
}

tcs_t tcs_diff(tcs_t from, tcs_t to) {
  //return from <= to ? to - from : TCS_MAX - from + to;
  return to - from;
}
