#ifndef __PERSIST_H__
#define __PERSIST_H__

#include <stdint.h>

#define PERSIST_SUCCESS 1
#define PERSIST_FAILURE 0

#define persist_load(var) __persist_load__((persist_data_t*)var, sizeof(*(var)))
#define persist_save(var) __persist_save__((const persist_data_t*)var, sizeof(*(var)))

typedef uint8_t persist_data_t;
typedef uint16_t persist_size_t;

int __persist_load__(persist_data_t *data, persist_size_t size);
int __persist_save__(const persist_data_t *data, persist_size_t size);

#endif /* __PERSIST_H__ */
