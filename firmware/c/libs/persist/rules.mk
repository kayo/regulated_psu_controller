persist.BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

TARGET.LIBS += libpersist
libpersist.INHERIT ?= firmware hardware
libpersist.CDIRS := $(persist.BASEPATH)include
libpersist.SRCS := $(patsubst %,$(persist.BASEPATH)src/%.c,persist)
