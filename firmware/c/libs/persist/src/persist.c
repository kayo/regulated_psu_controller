#include "persist.h"
#include <hw.h>
#include <string.h>

#define start ((persist_data_t*)&_par_start)
#define end ((persist_data_t*)&_par_end)

#define NONE_SIZE 0xff
#define read_size(addr) (*(const persist_size_t*)(addr))

#define word_align(ptr) (((ptr) + ((FLASH_WORD_SIZE) - 1)) & ~((FLASH_WORD_SIZE) - 1))

static persist_data_t *cell_addr = start;

int __persist_load__(persist_data_t *data, persist_size_t size) {
  const persist_data_t *data_addr = NULL;

  for (; cell_addr < end; ) {
    persist_size_t cell_size = read_size(cell_addr);

    if (cell_size == NONE_SIZE) {
      break;
    }

    if (cell_size == size) { /* we have stored data */
      data_addr = cell_addr + sizeof(persist_size_t); /* store it address */
    }

    cell_addr += word_align(sizeof(persist_size_t) + cell_size);
  }

  if (data_addr != NULL) {
    memcpy(data, data_addr, size);
    return PERSIST_SUCCESS;
  }

  return PERSIST_FAILURE;
}

int __persist_save__(const persist_data_t *data, persist_size_t size) {
  flash_unlock(start);

  if (cell_addr >= end) {
    flash_erase(start, end - start);
    cell_addr = start;
  }

#define head_size sizeof(persist_size_t)
#define aligned_head_size word_align(head_size)
#define extra_size (aligned_head_size - head_size)

  if (extra_size > 0) {
    union {
      persist_size_t size;
      uint8_t data[aligned_head_size];
    } tmp;

    tmp.size = size;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wtype-limits"
    memcpy(tmp.data + head_size, data, size < extra_size ? size : extra_size);
#pragma GCC diagnostic pop

    /* write first flash word */
    flash_write(cell_addr, tmp.data, aligned_head_size);

    if (size > extra_size) { /* when we have additional data, write it too */
      flash_write(cell_addr + aligned_head_size, data + extra_size, word_align(size - extra_size));
    }
  } else {
    /* write data size */
    flash_write(cell_addr, &size, head_size);

    /* write data */
    flash_write(cell_addr + head_size, data, word_align(size));
  }

  cell_addr += word_align(head_size + size);

  flash_lock(start);

  return PERSIST_SUCCESS;
}
