#ifndef __DISPLAY__PCD8544_H__
#define __DISPLAY__PCD8544_H__

#include <stdint.h>
#include <stdbool.h>

#define pcd8544_columns 84
#define pcd8544_rows 48
#define pcd8544_frame_pixels (pcd8544_columns * pcd8544_rows)
#define pcd8544_frame_bytes (pcd8544_frame_pixels / 8)

extern uint8_t pcd8544_frame_buffer[pcd8544_frame_bytes];

void pcd8544_init(void);
void pcd8544_done(void);

void pcd8544_update(void);
bool pcd8544_active(void);

/* external interface */
extern void pcd8544_set_command(void);
extern void pcd8544_set_data(void);
extern void pcd8544_spi_ptr(const uint8_t *ptr);
extern void pcd8544_spi_len(uint16_t len);
extern void pcd8544_spi_off(void);
extern void pcd8544_spi_on(void);
extern bool pcd8544_spi_state(void);

#define pcd8544_def(dc_pin, spi_dev)         \
  void pcd8544_set_command(void) {           \
    pio_##dc_pin##_clr();                    \
  }                                          \
                                             \
  void pcd8544_set_data(void) {              \
    pio_##dc_pin##_set();                    \
  }                                          \
                                             \
  void pcd8544_spi_ptr(const uint8_t *ptr) { \
    spi_dev##_tx_dma_set_ptr(ptr);           \
  }                                          \
                                             \
  void pcd8544_spi_len(uint16_t len) {       \
    spi_dev##_tx_dma_set_len(len);           \
  }                                          \
                                             \
  void pcd8544_spi_off(void) {               \
    spi_dev##_tx_dma_off();                  \
  }                                          \
                                             \
  void pcd8544_spi_on(void) {                \
    spi_dev##_tx_dma_on();                   \
  }                                          \
                                             \
  bool pcd8544_spi_state(void) {             \
    return spi_dev##_tx_dma_state();         \
  }

#endif /* __DISPLAY__PCD8544_H__ */
