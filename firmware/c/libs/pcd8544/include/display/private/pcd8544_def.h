#ifndef __DISPLAY__PRIVATE__PCD8544_DEF_H__
#define __DISPLAY__PRIVATE__PCD8544_DEF_H__

/**
 * @brief NOP instruction
 */
#define PCD8544_NOP 0

/**
 * @brief Function set instruction
 *
 * @param PD Power down control
 * @param V The entry mode
 * @param H Extended instruction set control
 */
#define PCD8544_FUNCTION_SET(PD, V, H) ((1<<5) | ((PD) << 2) | ((V) << 1) | (H))

/**
 * @brief Power on
 */
#define PCD8544_POWER_ON 0

/**
 * @brief Power off
 */
#define PCD8544_POWER_OFF 1

/**
 * @brief Horizontal addressing
 */
#define PCD8544_HORIZONTAL 0

/**
 * @brief Vertical addressing
 */
#define PCD8544_VERTICAL 1

/**
 * @brief Use basic instruction set
 */
#define PCD8544_BASIC 0

/**
 * @brief Use extended instruction set
 */
#define PCD8544_EXTENDED 1

/**
 * @brief Display control instruction
 * 
 * Sets display configuration.
 *
 * @param DE The display mode to use
 */
#define PCD8544_DISPLAY_CONTROL(DE) ((1<<3) | (DE))

/**
 * @brief Display blank
 */
#define PCD8544_BLANK 0x0

/**
 * @brief Normal display mode
 */
#define PCD8544_NORMAL 0x4

/**
 * @brief Display filled
 *
 * All display segments is on.
 */
#define PCD8544_FILLED 0x1

/**
 * @brief Inverse display mode
 */
#define PCD8544_INVERSE 0x5

/**
 * @brief Set Y address instruction
 * 
 * Sets Y address of RAM.
 *
 * @param Y The Y address [0...5]
 */
#define PCD8544_SET_Y_ADDRESS(Y) ((1<<6) | ((Y) & 0x07))

/**
 * @brief Set X address instruction
 * 
 * Sets X address of RAM.
 *
 * @param X The X address [0...83]
 */
#define PCD8544_SET_X_ADDRESS(X) ((1<<7) | ((X) & 0x7f))

/**
 * @brief Temperature control extended instruction
 * 
 * Set Temperature Coefficient (TC).
 *
 * @param TC The temperature coefficient [0...3]
 */
#define PCD8544_TEMPERATURE_CONTROL(TC) ((1<<2) | ((TC) & 0x03))

/**
 * @brief Bias system extended instruction
 * 
 * Sets Bias System (BS).
 *
 * @param BS The bias system [0...7]
 */
#define PCD8544_BIAS_SYSTEM(BS) ((1<<4) | ((BS) & 0x07))

#define PCD8544_BIAS_N7_1S100 0x0
#define PCD8544_BIAS_N6_1S80 0x1
#define PCD8544_BIAS_N5_1S65_1S65 0x2
#define PCD8544_BIAS_N4_1S48 0x3
#define PCD8544_BIAS_N3_1S40_1S34 0x4
#define PCD8544_BIAS_N2_1S24 0x5
#define PCD8544_BIAS_N1_1S18_1S16 0x6
#define PCD8544_BIAS_N0_1S10_1S9_1S8 0x7

/**
 * @brief Operation voltage extended instruction
 * 
 * Sets operation voltage (Vop).
 *
 * @param Vop The operation voltage to set
 */
#define PCD8544_OPERATION_VOLTAGE(Vop) ((1<<7) | ((Vop) & 0x7f))

#endif /* __DISPLAY__PRIVATE__PCD8544_DEF_H__ */
