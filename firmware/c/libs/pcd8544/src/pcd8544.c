#include <display/private/pcd8544_def.h>
#include <display/pcd8544.h>
#include <type/ustr.h>

#ifndef NOINIT_ATTR
#define NOINIT_ATTR __attribute__((section(".noinit")))
#endif

uint8_t NOINIT_ATTR pcd8544_frame_buffer[pcd8544_frame_bytes];

static int cmd_resize(ustr_t *str, ulen_t len) {
  if (len > pcd8544_frame_bytes) {
    return -1;
  } else if (len == 0) {
    ustr_len(str) = 0;
  }

  return 0;
}

static inline void init_commands(ustr_t *cmd) {
  ustr_extern_init(cmd, pcd8544_frame_buffer);
  cmd->res = cmd_resize;
}
static inline void put_command(ustr_t *cmd, uint8_t val) {
  const uint8_t var = (val);
  ustr_putvr(cmd, var);
}

static inline void send_commands(ustr_t *cmd) {
  pcd8544_set_command();
  pcd8544_spi_len(ustr_len(cmd));
  pcd8544_spi_on();
}

void pcd8544_init(void){
  pcd8544_spi_ptr(pcd8544_frame_buffer);
  
  ustr_t cmd;
  init_commands(&cmd);

  put_command(&cmd, PCD8544_FUNCTION_SET(PCD8544_POWER_ON, PCD8544_HORIZONTAL, PCD8544_EXTENDED));
  put_command(&cmd, PCD8544_BIAS_SYSTEM(PCD8544_BIAS_N4_1S48));
  put_command(&cmd, PCD8544_OPERATION_VOLTAGE(64));
  put_command(&cmd, PCD8544_TEMPERATURE_CONTROL(0));

  put_command(&cmd, PCD8544_FUNCTION_SET(PCD8544_POWER_ON, PCD8544_HORIZONTAL, PCD8544_BASIC));
  put_command(&cmd, PCD8544_DISPLAY_CONTROL(PCD8544_NORMAL));
  put_command(&cmd, PCD8544_SET_X_ADDRESS(0));
  put_command(&cmd, PCD8544_SET_Y_ADDRESS(0));

  send_commands(&cmd);
}

void pcd8544_done(void) {
  ustr_t cmd;
  init_commands(&cmd);

  put_command(&cmd, PCD8544_FUNCTION_SET(PCD8544_POWER_OFF, PCD8544_HORIZONTAL, PCD8544_BASIC));
  send_commands(&cmd);
}

bool pcd8544_active(void) {
  return pcd8544_spi_state();
}

void pcd8544_update(void) {
  if (pcd8544_active()) return;
  
  pcd8544_set_data();
  pcd8544_spi_off();
  pcd8544_spi_len(sizeof(pcd8544_frame_buffer));
  pcd8544_spi_on();
}
