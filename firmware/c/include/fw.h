#ifndef FW_H
#define FW_H

#define t(s) (s)

enum {
  any_ev = 0,
  sys_ev_time = 1 << 0,
  usr_ev_push = 1 << 2,
  usr_ev_hold = 1 << 3,
  usr_ev_pull = 1 << 4,
  usr_ev_incr = 1 << 5,
  usr_ev_decr = 1 << 6,
  usr_ev = usr_ev_push | usr_ev_hold | usr_ev_pull | usr_ev_incr | usr_ev_decr,
};

#define main_resume()                    \
    mcu_modes_on_off(mcu_modes_notouch,  \
                     mcu_sleep_on_exit)

#define main_suspend()                 \
  mcu_modes_on_off(mcu_sleep_on_exit,  \
                   mcu_modes_notouch); \
  mcu_wait_for_interrupt()

#define emit(ev) {              \
    /* mcu_atomic_context(); */ \
    evt_set(events, ev);        \
  }

#define wait(ev)                        \
  for (uint8_t selected = wait_for(ev); \
       selected != 0; selected = 0)

#define on(ev) \
  evt_on(selected, ev)

extern volatile uint8_t events;

static inline void
__exit_emitter_context__(uint8_t *var) {
  (void)var;
  if (events != 0) {
    main_resume();
  }
}

#define emitter_context()                                 \
  uint8_t __emitter_exit_var__                            \
  __attribute__((__cleanup__(__exit_emitter_context__)));

#define timer_set(x)              \
  /* mcu_atomic_block() */ {      \
    /*evt_clr(events, sys_ev_time); */ \
    tmr_set(timer, x);            \
  }

#define timer_wait(t) {      \
    timer_set(t);            \
    wait_for(sys_ev_time);   \
  }

#define temp_valid(val)                             \
  (                                                 \
   gen_ge(VAL_T, val, gen_make(VAL_T, T_MIN)) &&    \
   gen_le(VAL_T, val, gen_make(VAL_T, T_MAX))       \
  )

extern VAL_T U, I;
extern VAL_T T0, T1;
extern VAL_T Dfan;

#define CTL_PERIOD_VAL gen_make(VAL_T, CTL_PERIOD)

extern void ctl_init(void);
extern void ctl_step(void);

#endif /* FW_H */
