#ifndef THERMISTOR_H
#define THERMISTOR_H

/**
 * @brief Convert ADC value to resistance.
 *
 *        Analog VCC
 *
 *            ^
 *            |
 *        .---o---.
 *        |       |
 *       .-.     .-.
 *       | |     | |
 *    Rp | |  Rt | |
 *       | |     | |
 *       '-'     '-'
 *        |       |
 *        '---o---o----> Analog IN
 *            |
 *           .-.
 *           | |
 *        Rs | |
 *           | |
 *           '-'
 *           _|_
 *
 *        Analog GND
 *
 * If Rs doesn't used, Rs must be set to 0
 */
#define D2R_HI_WITH_Rp(D, Rs, Rp) ((1.0 - (D)) * (Rs) * (Rp) / (((D) - 1.0) * (Rs) + (Rp)))
#define D2R_HI_WITHOUT_Rp(D, Rs) ((1.0 - (D)) * (Rs) / (D))
#define A2R_HI(A, Rs, Rp) ((Rp) == 0 ? D2R_HI_WITHOUT_Rp(A2D(A), Rs) : D2R_HI_WITH_Rp(A2D(A), Rs, Rp))

#define R2D_HI_WITH_Rp(R, Rs, Rp) ((((double)(Rs) - (R)) * (Rp) + (double)(R) * (Rs)) / ((Rs) * ((Rp) + (R))))
#define R2D_HI_WITHOUT_Rp(R, Rs) ((double)(Rs) / ((Rs) + (R)))
#define R2A_HI(R, Rs, Rp) D2A((Rp) == 0 ? R2D_HI_WITHOUT_Rp(R, Rs) : R2D_HI_WITH_Rp(R, Rs, Rp))

/**
 * @brief Steinhart-Hart NTC thermistor model definition
 *
 * [e.illumium.org/thermistor](http://e.illumium.org/thermistor)
 */
#define SH_R2K(l, a, b, c) (1.0 / ((a) + (b) * (l) + (c) * pow((l), 3)))

/**
 * @brief Simplified beta NTC thermistor model definition
 *
 * [e.illumium.org/thermistor](http://e.illumium.org/thermistor)
 */
#define BETA_R2K(R, BETA, R0, T0) (1.0 / (1.0 / (T0) + 1.0 / (BETA) * log((R) / (R0))))
#define BETA_K2R(K, BETA, R0, T0) (R0 * exp(BETA / K - BETA / T0))

/**
 * @brief Kelvins to Celsius
 */
#define K2C(K) ((K)-273.15)

/**
 * @brief Celsius to Kelvins
 */
#define C2K(C) ((C)+273.15)

#endif /* THERMISTOR_H */
