# Compiler
COMPILER_NAME ?= arm-none-eabi-
GDBREMOTE ?= localhost:3333

# Base path to build root
BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

LIBSDIR := $(BASEPATH)libs
MKDIR := $(LIBSDIR)/mcuhw/rules
INCDIR := $(BASEPATH)include
SRCDIR := $(BASEPATH)src

-include config.mk

# Common
include $(MKDIR)/macro.mk
include $(MKDIR)/option.mk
include $(MKDIR)/hwdef.mk
include $(MKDIR)/build.mk
include $(MKDIR)/openocd.mk
include $(MKDIR)/image.mk
include $(MKDIR)/stalin.mk
include $(MKDIR)/cortex.mk

OCD_TARGET := stm32f0x
firmware.INHERIT := stalin $(call cortex-from-target,stm32f0)
$(call use_toolchain,firmware,$(COMPILER_NAME))

TARGET.LIBS += hardware
TARGET.HWDS += hardware
hardware.INHERIT := firmware
hardware.HWDS := $(SRCDIR)/hw.def
hardware.CDEFS := HWDEF_HEADER='<hw.h>'

libhandler.events := 4
include $(LIBSDIR)/handler/rules.mk
include $(LIBSDIR)/pcd8544/rules.mk
include $(LIBSDIR)/ucontrol/rules.mk
include $(LIBSDIR)/uprimitive/rules.mk
include $(LIBSDIR)/persist/rules.mk

TARGET.LIBS += libmain
libmain.INHERIT := hardware libprimitive libcontrol libhandler libpcd8544 libpersist
libmain.CDIRS := $(INCDIR)
libmain.SRCS := $(addprefix $(SRCDIR)/,\
  fw.c \
  sensor.c \
  driver.c \
  font_8x8.psf)
libmain.SIZEB = 2

TARGET.OPTS += libmain
libmain.OPTS := $(SRCDIR)/fw.cf

TARGET.CTLS += libmain
libmain.CTLS := $(SRCDIR)/ctl.def

TARGET.IMGS += main
main.INHERIT := libmain
main.DEPLIBS* := hardware libmain libprimitive libhandler libpcd8544 libpersist

# Provide rules
$(call ADDRULES,\
OPT_RULES:TARGET.OPTS\
CTL_RULES:TARGET.CTLS\
HWD_RULES:TARGET.HWDS\
LIB_RULES:TARGET.LIBS\
BIN_RULES:TARGET.BINS\
IMG_RULES:TARGET.IMGS)
