#include <ctl/type.h>
#include <ctl/util.h>
#include <ctl/ewma.h>
#include <ctl/med.h>

#include "hw.h"
#include "fw.h"

#define ADC_PROC_SAMPLES (ADC_SAMPLES / 2)

#define A2D(A) (((double)(A) - (double)(ADC_MIN)) * (1.0 / ((double)(ADC_MAX) - (double)(ADC_MIN))))
#define D2A(D) ((D) * ((double)(ADC_MAX) - (double)(ADC_MIN)) + (double)(ADC_MIN))

#include "thermistor.h"

#define T_A2R(A) A2R_HI(A, T_RS, T_RP)
#define T_R2A(R) R2A_HI(R, T_RS, T_RP)

#define T_T2A(C) T_R2A(BETA_K2R(C2K(C), T_BETA, T_RN, C2K(T_NOM)))
#define T_A2T(A) K2C(BETA_R2K(T_A2R(A), T_BETA, T_RN, C2K(T_NOM)))

#include "ctl.h"

#define toT(type, A) lookup_val(type, T, A)

static const ewma_param_t(VAL_T) T_flt_param = ewma_make_N(VAL_T, ADC_PROC_SAMPLES);

#define Tflt_state(TX) TX##_flt_state

static ewma_state_t(VAL_T) Tflt_state(T0), Tflt_state(T1);

#define Tflt_init(TX) ewma_init(VAL_T, &TX##_flt_state, TX = raw_##TX)
#define Tflt_step(TX, PX) TX = ewma_step(VAL_T, &PX##_flt_param, &TX##_flt_state, raw_##TX)

VAL_T T0, T1;

bool inited = false;

static void adc_conv(VAL_T data[]) {
  //int__Uref = toU(VAL_T, data[ADC_INDEX_U_REF]);
  //int__Tmcu = toTmcu(VAL_T, data[ADC_INDEX_T_MCU]);

  VAL_T raw_T0 = toT(VAL_T, data[ADC_INDEX_T0_SENS]);
  VAL_T raw_T1 = toT(VAL_T, data[ADC_INDEX_T1_SENS]);

  if (inited) {
    Tflt_step(T0, T);
    Tflt_step(T1, T);
  } else {
    Tflt_init(T0);
    Tflt_init(T1);

    inited = true;
  }
}

static void adc_proc(const adc_channels_t *src) {
  VAL_T data[ADC_CHANNELS];
  uint8_t sample;
  uint8_t channel;

  for (channel = 0; channel < ADC_CHANNELS; channel ++) {
    VAL_T buf[ADC_PROC_SAMPLES];

    for (sample = 0; sample < ADC_PROC_SAMPLES; sample++) {
      buf[sample] = gen_make(VAL_T, src[sample]._[channel]);
    }

    data[channel] = scale_val_const(VAL_T,
                                    med_step(VAL_T, buf, ADC_PROC_SAMPLES, ADC_PROC_SAMPLES / 3),
                                    ADC_MIN, ADC_MAX,
                                    0, (1<<12)-1);
  }

  adc_conv(data);
}

void adc_dma_on_half(void) {
  adc_proc(&adc_dma_data[0]);
}

void adc_dma_on_full(void) {
  adc_proc(&adc_dma_data[ADC_PROC_SAMPLES]);
}
