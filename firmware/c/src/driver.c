#include <ctl/type.h>
#include <ctl/util.h>
#include <ctl/pid.h>

#include "hw.h"
#include "fw.h"

static const pid_param_t(VAL_T) fan_pid_param =
  pid_make_Kp_Ti_Td_Ei_limit(VAL_T,
                             gen_make(VAL_T, FAN_PID_KP),
                             gen_make(VAL_T, FAN_PID_TI),
                             gen_make(VAL_T, FAN_PID_TD),
                             gen_make(VAL_T, FAN_PID_EI_LIM),
                             CTL_PERIOD_VAL);
pid_state_t(VAL_T) fan_pid_state;

#define toM(val)                                     \
  gen_int(VAL_T,                                     \
          scale_val_const(VAL_T,                     \
                          clamp_val_const(VAL_T,     \
                                          val,       \
                                          0.0, 1.0), \
                          0.0, 1.0,                  \
                          0, FAN_TIM_TOP - 1))

void ctl_init(void) {
  pid_init(VAL_T, &fan_pid_state);
}

VAL_T Dfan;

void ctl_step(void) {
  if (temp_valid(T0) || temp_valid(T1)) { /* the heatsink temperature is in a valid range */
    /* use PID control for fan speed regulation */
    VAL_T fan_Ths_error = error_val(VAL_T,
                                    gen_make(VAL_T, T_NORM),
                                    max_val(VAL_T, T0, T1));

    VAL_T Dfan_raw = pid_step(VAL_T, &fan_pid_param, &fan_pid_state, fan_Ths_error);

    Dfan = gen_gt(VAL_T, Dfan_raw, gen_0(VAL_T)) ?
      clamp_val_const(VAL_T, Dfan_raw, FAN_MIN, FAN_MAX) :
      gen_0(VAL_T);
  } else { /* unable to determine heatsink temperature */
    Dfan = gen_make(VAL_T, FAN_MAX); /* turn on fan to avoid power key damage */
  }

  fan_tim_FAN_PWM_put(toM(Dfan));
}
