#include <ctl/type.h>
#include <ctl/util.h>

#include "hw.h"
#include "fw.h"

#include <handler/evt.h>
#include <handler/tmr.h>
#include <handler/btn.h>
#include <handler/enc.h>
#include <display/bitmap.h>
#include <display/pcd8544.h>
#include <type/ustr.h>
#include <persist.h>

typedef enum {
  mode_off = 0,
  mode_on = 1,
} mode_t;

typedef enum {
  state_none = 0,
  state_tune_U = 1,
  state_tune_I = 2,
} state_t;

typedef struct {
  uint16_t U; /* Voltage */
  uint16_t I; /* Current */
  uint8_t mode; /* Mode of operation */
} settings_t;

#define U_LIMIT ((uint16_t)((float)(U_MAX - U_MIN) / (float)U_STEP))
#define I_LIMIT ((uint16_t)((float)(I_MAX - I_MIN) / (float)I_STEP))

static state_t state = state_none; /* current state */
static settings_t settings = { /* current settings */
  .U = ((uint16_t)((float)(U_DEF - U_MIN) / (float)U_STEP)),
  .I = ((uint16_t)((float)(I_DEF - I_MIN) / (float)I_STEP)),
  .mode = mode_off,
};

btn_def(ENC_P);
enc_def(ENC, emit(usr_ev_incr), emit(usr_ev_decr));
pcd8544_def(LCD_DC, spi);

static bitmap_state_t canvas_state;

static const bitmap_canvas_t canvas = {
  pcd8544_frame_buffer,
  { {{ 0, 0 }},
    {{ 84, 48 }} },
  &canvas_state
};

static void dsp_init(void) {
  bitmap_init(&canvas);

  bitmap_primitive(&canvas, bitmap_line);
  bitmap_align(&canvas, bitmap_top | bitmap_left);
  bitmap_font(&canvas, font_8x8);

  pcd8544_init();
}

static void dsp_bl(fix16 val) {
  lcd_tim_LCD_PWM_put(gen_int(VAL_T,
                              scale_val_const(VAL_T,
                                              clamp_val_const(VAL_T,
                                                              val,
                                                              0.0, 1.0),
                                              0.0, 1.0,
                                              0, LCD_TIM_TOP - 1)));
}

#define toM(VAL, HW_MAX)                                            \
  gen_int(VAL_T,                                                    \
          gen_mul(VAL_T, VAL,                                       \
                  gen_make(VAL_T,                                   \
                           ((float)REG_TIM_TOP) / (float)HW_MAX)))

static void update(void) {
  U = gen_add(VAL_T, settings.U * gen_make(VAL_T, U_STEP), gen_make(VAL_T, U_MIN));
  I = gen_add(VAL_T, settings.I * gen_make(VAL_T, I_STEP), gen_make(VAL_T, I_MIN));

  if (settings.mode == mode_on) {
    reg_tim_U_PWM_put(toM(U, HW_U_MAX));
    reg_tim_I_PWM_put(toM(I, HW_I_MAX));
  } else {
    reg_tim_U_PWM_put(toM(gen_make(VAL_T, HW_U_MIN), HW_U_MAX));
    reg_tim_I_PWM_put(toM(gen_make(VAL_T, HW_I_MIN), HW_I_MAX));
  }
}

static volatile uint16_t timer;

#define TICK_PERIOD (1000/STK_CLK)

void clk_on_tick(void) {
  emitter_context();

  //iwd_renew();

  btn_on(ENC_P, TICK_PERIOD) {
    btn_released(ENC_P, 20, 700) {
      emit(usr_ev_push);
    }
    btn_pushed(ENC_P, 1000) {
      emit(usr_ev_hold);
    }
  }

  tmr_on(timer, TICK_PERIOD) {
    emit(sys_ev_time);
  }

  ctl_step();
}

static uint8_t wait_for(uint8_t required) {
  for (;;) {
    uint8_t selected = evt_get(events, required == 0 ? 0xff : required);

    if (selected != 0) {
      return selected;
    }

    main_suspend();
  }
}

#define row(x) ((x)*9+1)
#define col(x) ((x)*8+2)

static void bitmap_val(const bitmap_canvas_t *const c,
                       bitmap_coord_t x, bitmap_coord_t y,
                       bitmap_coord_t X, bitmap_coord_t Y,
                       VAL_T v, uint8_t p, bitmap_coord_t l) {
  ustr_static(b, 8);
  ustr_t t;

  ustr_static_init(&t, &b);
  x += l - col(ustr_putsfz(&t, v, 16, p));
  bitmap_text(c, x, y, X, Y, ustr_str(&t));
}

VAL_T U;
VAL_T I;

static void ui_main(void) {
  for (;;) {
    bitmap_primitive(&canvas, bitmap_fill);

    bitmap_operation(&canvas, bitmap_clear);
    bitmap_apply(&canvas);

    bitmap_operation(&canvas, bitmap_draw);
    /*bitmap_rect(&canvas, col(0)-2, row(0)-2, col(9)+2, row(1)-1);
      bitmap_operation(&canvas, bitmap_clear);*/

    bitmap_text(&canvas, col(0), row(0), 0, 0,
                settings.mode == mode_on ? t("Включено") : t("Выключено"));

    bitmap_operation(&canvas, bitmap_draw);

    if (state == state_tune_U) {
      bitmap_rect(&canvas, col(0)-2, row(1)-2, col(4)+2, row(2)-1);
      bitmap_operation(&canvas, bitmap_clear);
    }

    bitmap_val (&canvas, col(0), row(1), 0, 0, U, 1, col(4));

    bitmap_operation(&canvas, bitmap_draw);
    bitmap_text(&canvas, col(5), row(1), 0, 0, t("Вольт"));

    if (state == state_tune_I) {
      bitmap_rect(&canvas, col(0)-2, row(2)-2, col(4)+2, row(3)-1);
      bitmap_operation(&canvas, bitmap_clear);
    }

    bitmap_val (&canvas, col(0), row(2), 0, 0, I, 1, col(4));

    bitmap_operation(&canvas, bitmap_draw);
    bitmap_text(&canvas, col(5), row(2), 0, 0, t("Ампер"));

    bitmap_operation(&canvas, bitmap_draw);

    bitmap_val (&canvas, col(0), row(3), 0, 0, gen_mul(VAL_T, I, U), 1, col(4));
    bitmap_text(&canvas, col(5), row(3), 0, 0, t("Ватт"));

#define show_temp(COL, ROW, TX)                                       \
    if (temp_valid(TX)) {                                             \
      bitmap_val (&canvas, col(COL), row(ROW), 0, 0, TX, 0, col(3));  \
    } else {                                                          \
      bitmap_text(&canvas, col(COL), row(ROW), 0, 0, t("---"));       \
    }

    show_temp(0, 4, max_val(VAL_T, T0, T1));
    //show_temp(4, 4, T1);
    bitmap_text(&canvas, col(4), row(4), 0, 0, t("C"));

    //bitmap_text(&canvas, col(0), row(4), 0, 0, t("Обдув"));
    bitmap_val (&canvas, col(6), row(4), 0, 0, Dfan * 100, 0, col(3));
    bitmap_text(&canvas, col(9), row(4), 0, 0, t("%"));

    timer_set(1000);
    pcd8544_update();

    wait(any_ev) {
      on(usr_ev_hold) { /* on/off by holding rotary encoder button in any state */
        settings.mode = settings.mode == mode_on ? mode_off : mode_on;
        update();
        persist_save(&settings);
      }

      on(usr_ev_push) { /* switch state by pushing rotary encoder button */
        state = state == state_none ? state_tune_U :
          state == state_tune_U ? state_tune_I :
          state_none;

        if (state == state_none) { /* store settings on exit */
          persist_save(&settings);
        }
      }

      switch (state) {
      case state_none:
        break;
      case state_tune_U:
        on(usr_ev_incr) {
          if (settings.U < U_LIMIT - 1) {
            settings.U += 1;
            update();
          }
        }
        on(usr_ev_decr) {
          if (settings.U > 0) {
            settings.U -= 1;
            update();
          }
        }
        break;
      case state_tune_I:
        on(usr_ev_incr) {
          if (settings.I < I_LIMIT - 1) {
            settings.I += 1;
            update();
          }
        }
        on(usr_ev_decr) {
          if (settings.I > 0) {
            settings.I -= 1;
            update();
          }
        }
        break;
      }
    }
  }
}

static void ui_init(void) {
  bitmap_operation(&canvas, bitmap_clear);
  bitmap_apply(&canvas);

  bitmap_operation(&canvas, bitmap_draw);

  bitmap_text(&canvas, 4, 20, 0, 0, "Запуск...");
  pcd8544_update();

  timer_wait(500);
}

volatile uint8_t events;

int main(void) {
  irq_init();
  clk_init();
  pio_init();

  adc_init();
  reg_tim_init();
  fan_tim_init();
  lcd_tim_init();
  spi_init();

  btn_ENC_P_init();
  enc_ENC_init();

  /* setup display */
  timer_wait(500);
  dsp_init();
  dsp_bl(gen_make(VAL_T, 0.2));
  timer_wait(10);

  /* start watchdog */
  //iwd_start();
  //iwd_renew();

  ctl_init();
  persist_load(&settings);
  update();

  /* show ui */
  ui_init();
  ui_main();

  pcd8544_done();
  spi_done();
  reg_tim_done();
  fan_tim_done();
  lcd_tim_done();

  adc_done();

  pio_done();
  clk_done();
  irq_done();

  return 0;
}
